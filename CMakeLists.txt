# PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
# Copyright (C) 2022 mrkubax10

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

project(PBEditor C)
add_executable(PBEditor
	src/main.c
	src/app.c
	src/editor.c
	src/data.c
	src/overlay.c
	src/menu.c
	src/overview.c
	src/binary_utils.c
)
if(WIN32)
	target_link_libraries(PBEditor PRIVATE "mingw32")
	target_link_libraries(PBEditor PRIVATE "SDL2main")
endif()
target_link_libraries(PBEditor PRIVATE "SDL2")
target_link_libraries(PBEditor PRIVATE "SDL2_image")
target_link_libraries(PBEditor PRIVATE "SDL2_ttf")
target_link_libraries(PBEditor PRIVATE "m")
target_include_directories(PBEditor PRIVATE "${PROJECT_SOURCE_DIR}/src")
