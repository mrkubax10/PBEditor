// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_EDITOR_H
#define SRC_EDITOR_H

#include <stdint.h>

#include "overlay.h"
#include "menu.h"

#define SECTOR_WIDTH 32
#define SECTOR_HEIGHT 18
#define TILE_COUNT 278
#define TILE_SIZE 10

typedef struct App App;

typedef struct Editor{
	SDL_Renderer* renderer;
	uint16_t sector[SECTOR_HEIGHT*SECTOR_WIDTH];
	uint16_t sectorBackgroundID;
	uint16_t sectorMusicID;
	uint16_t sectorUnknown1;
	char* sectorMetadata;
	unsigned sectorMetadataSize;
	SDL_Texture* tileset;
	SDL_Texture* background;
	App* app;
	Overlay overlay;
	Menu editorMenu;
	int sectorx;
	int sectory;
} Editor;
void Editor_init(Editor* self,SDL_Renderer* renderer,App* app);
void Editor_delete(Editor* self);
void Editor_update(Editor* self,SDL_Event* event);
void Editor_draw(Editor* self);
void Editor_openSector(Editor* self,int x,int y);
void Editor_save(Editor* self);
void Editor_reloadBackground(Editor* self);
SDL_Surface* Editor_createBackground(Editor* self,SDL_Surface* backgroundUnit);
SDL_Surface* Editor_generateTilesetSurface(Editor* self);

#endif
