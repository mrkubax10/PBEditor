// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "overlay.h"

#include <SDL2/SDL_image.h>

#include "binary_utils.h"
#include "data.h"
#include "editor.h"
#include "app.h"
#include "menu.h"

#define MUSIC_COUNT 19
#define BACKGROUND_COUNT 30

static Editor* globalEditor=0;

static void MusicMenu_changeMusic(void* arg){
	globalEditor->sectorMusicID=(uint32_t)(intptr_t)arg;
	Menu_close(&globalEditor->overlay.musicMenu);
}
static void Menu_cancel(void* arg){
	Menu_close((Menu*)arg);
}
static void BackgroundMenu_changeBackground(void* arg){
	globalEditor->sectorBackgroundID=(uint32_t)(intptr_t)arg;
	Editor_reloadBackground(globalEditor);
	Menu_close(&globalEditor->overlay.backgroundMenu);
}
static void StartFromHereConfimMenu_confirm(void* arg){
	Overlay* overlay=(Overlay*)arg;
	Overlay_startFromHere(overlay);
	Menu_close(&overlay->startFromHereConfirmMenu);
}
static void StartFromHereConfimMenu_cancel(void* arg){
	Overlay* overlay=(Overlay*)arg;
	Menu_close(&overlay->startFromHereConfirmMenu);
}

static SDL_Texture* Overlay_generateToolTexture(Overlay* self,const char* text){
	SDL_Surface* textureSurf=SDL_CreateRGBSurface(0,10,10,32,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);

	SDL_FillRect(textureSurf,0,SDL_MapRGB(textureSurf->format,80,80,80));

	SDL_Surface* textureIcon=TTF_RenderUTF8_Blended(self->app->font,text,(SDL_Color){255,255,255,255});
	SDL_Rect rect={0,0,10,10};
	SDL_BlitScaled(textureIcon,0,textureSurf,&rect);

	SDL_Texture* tex=SDL_CreateTextureFromSurface(self->renderer,textureSurf);
	SDL_FreeSurface(textureIcon);
	SDL_FreeSurface(textureSurf);

	return tex;
}
void Overlay_init(Overlay* self,SDL_Renderer* renderer,Editor* editor,App* app){
	self->renderer=renderer;
	self->editor=editor;
	self->app=app;
	self->overlayOffset=0;
	self->selectedTile=1;
	self->mouseDown=false;
	self->rubberTexture=Overlay_generateToolTexture(self,"R");
	self->musicTexture=Overlay_generateToolTexture(self,"M");
	self->backgroundTexture=Overlay_generateToolTexture(self,"B");
	char* startFromHereData;
	SDL_RWops* rw=Data_getFileRW("ASSETS/GRAFIKA/STRZALKA.png",&startFromHereData);
	SDL_Surface* startFromHereSurf=IMG_Load_RW(rw,0);
	SDL_FreeRW(rw);
	free(startFromHereData);
	self->startFromHereTexture=SDL_CreateTextureFromSurface(self->renderer,startFromHereSurf);
	SDL_FreeSurface(startFromHereSurf);

	Menu_init(&self->musicMenu,renderer,app);
	char nameStr[128];
	for(unsigned i=0; i<=MUSIC_COUNT; i++){
		snprintf(nameStr,128,"Music %d",i);
		Menu_addLabelItem(&self->musicMenu,nameStr,MusicMenu_changeMusic,(void*)(intptr_t)i);
	}
	Menu_addSeparatorItem(&self->musicMenu);
	Menu_addLabelItem(&self->musicMenu,"Cancel",Menu_cancel,(void*)&self->musicMenu);

	Menu_init(&self->backgroundMenu,renderer,app);
	for(unsigned i=0; i<=BACKGROUND_COUNT; i++){
		snprintf(nameStr,128,"Background %d",i);
		Menu_addLabelItem(&self->backgroundMenu,nameStr,BackgroundMenu_changeBackground,(void*)(intptr_t)i);
	}
	Menu_addSeparatorItem(&self->backgroundMenu);
	Menu_addLabelItem(&self->backgroundMenu,"Cancel",Menu_cancel,(void*)&self->backgroundMenu);

	Menu_init(&self->startFromHereConfirmMenu,renderer,app);
	Menu_addHeaderItem(&self->startFromHereConfirmMenu,"This action will change your savegame data. Are you sure?");
	Menu_addSeparatorItem(&self->startFromHereConfirmMenu);
	Menu_addLabelItem(&self->startFromHereConfirmMenu,"Yes",StartFromHereConfimMenu_confirm,(void*)self);
	Menu_addLabelItem(&self->startFromHereConfirmMenu,"No",StartFromHereConfimMenu_cancel,(void*)self);

	globalEditor=editor;
}
void Overlay_delete(Overlay* self){
	SDL_DestroyTexture(self->rubberTexture);
	SDL_DestroyTexture(self->musicTexture);
	SDL_DestroyTexture(self->backgroundTexture);
	SDL_DestroyTexture(self->startFromHereTexture);
}
void Overlay_draw(Overlay* self){
	SDL_Rect rect={320,0,20,180};
	SDL_SetRenderDrawColor(self->renderer,40,40,40,255);
	SDL_RenderFillRect(self->renderer,&rect);

	rect.x=0;
	rect.w=TILE_SIZE;
	rect.h=TILE_SIZE;
	SDL_Rect rect2;
	rect2.y=-TILE_SIZE;
	rect2.w=TILE_SIZE;
	rect2.h=TILE_SIZE;
	for(int i=self->overlayOffset; i<self->overlayOffset+SECTOR_HEIGHT*2; i++){
		rect.y=i*TILE_SIZE;
		rect2.x=320+(i%2)*TILE_SIZE;
		rect2.y+=TILE_SIZE-(i%2)*TILE_SIZE;
		SDL_RenderCopy(self->renderer,self->editor->tileset,&rect,&rect2);
	}

	rect.y=180;
	rect.w=340;
	rect.h=10;
	SDL_SetRenderDrawColor(self->renderer,40,40,40,255);
	SDL_RenderFillRect(self->renderer,&rect);

	rect.w=10;
	rect.h=10;
	SDL_RenderCopy(self->renderer,self->rubberTexture,0,&rect);

	rect.x+=10;
	SDL_RenderCopy(self->renderer,self->musicTexture,0,&rect);

	rect.x+=10;
	SDL_RenderCopy(self->renderer,self->backgroundTexture,0,&rect);

	rect.x+=10;
	SDL_RenderCopy(self->renderer,self->startFromHereTexture,0,&rect);

	if(!Menu_isMenuOpened() && self->app->logicalMousePos.x>=0 && self->app->logicalMousePos.x<320 && self->app->logicalMousePos.y>=0 && self->app->logicalMousePos.y<180){
		rect.x=0;
		rect.y=(self->selectedTile-1)*TILE_SIZE;
		rect.w=TILE_SIZE;
		rect.h=TILE_SIZE;	
		rect2.x=self->app->logicalMousePos.x/TILE_SIZE*TILE_SIZE;
		rect2.y=self->app->logicalMousePos.y/TILE_SIZE*TILE_SIZE;

		SDL_SetTextureAlphaMod(self->editor->tileset,128);
		SDL_RenderCopy(self->renderer,self->editor->tileset,&rect,&rect2);
		SDL_SetTextureAlphaMod(self->editor->tileset,255);

		if(self->mouseDown){
			unsigned x=self->app->logicalMousePos.x/TILE_SIZE;
			unsigned y=self->app->logicalMousePos.y/TILE_SIZE;
			self->editor->sector[y*SECTOR_WIDTH+x]=self->selectedTile;
		}
	}

	Menu_draw(&self->musicMenu);
	Menu_draw(&self->backgroundMenu);
	Menu_draw(&self->startFromHereConfirmMenu);
}
void Overlay_update(Overlay* self,SDL_Event* event){
	if(event->type==SDL_MOUSEWHEEL){
		if(self->app->logicalMousePos.x>=320 && self->app->logicalMousePos.x<=340 && self->app->logicalMousePos.y>=0 && self->app->logicalMousePos.y<=180){
			self->overlayOffset-=event->wheel.y*2;
			if(self->overlayOffset>TILE_COUNT-SECTOR_HEIGHT*2)
				self->overlayOffset=TILE_COUNT-SECTOR_HEIGHT*2;
			else if(self->overlayOffset<0)
				self->overlayOffset=0;
		}
	}
	else if(event->type==SDL_MOUSEBUTTONDOWN && event->button.button==SDL_BUTTON_LEFT){
		if(self->app->logicalMousePos.x>=320 && self->app->logicalMousePos.x<=340 && self->app->logicalMousePos.y>=0 && self->app->logicalMousePos.y<=180){
			unsigned xoffset=(unsigned)self->app->logicalMousePos.x-320;
			self->selectedTile=self->app->logicalMousePos.y/TILE_SIZE*2+((xoffset/TILE_SIZE)%2)+self->overlayOffset+1;
		}
		else if(self->app->logicalMousePos.y>=180){
			unsigned toolbarElement=floorf(self->app->logicalMousePos.x/10.0f);
			switch(toolbarElement){
			case 0: // Rubber
				self->selectedTile=0;
				break;
			case 1: // Music
				Menu_open(&self->musicMenu);
				break;
			case 2: // Background
				Menu_open(&self->backgroundMenu);
				break;
			case 3: // Start from here
				Menu_open(&self->startFromHereConfirmMenu);
				break;
			}
		}
		self->mouseDown=true;
	}
	else if(event->type==SDL_MOUSEBUTTONUP && event->button.button==SDL_BUTTON_LEFT)
		self->mouseDown=false;

	Menu_update(&self->musicMenu,event);
	Menu_update(&self->backgroundMenu,event);
	Menu_update(&self->startFromHereConfirmMenu,event);
}
void Overlay_startFromHere(Overlay *self){
	char* pref=SDL_GetPrefPath("","PerypetieBoba");
	unsigned savefilePathLength=strlen(pref)+strlen("savefile")+1;
	char savefilePath[savefilePathLength];
	snprintf(savefilePath,savefilePathLength,"%ssavefile",pref);

	FILE* file=fopen(savefilePath,"rb+");

	if(!file){
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING,"PBEditor","Failed to open savegame file, make sure that you started game at least once.",0);
		return;
	}

	char temp[4];
	uint32ToBytes((uint32_t)self->app->editor.sectorx,temp);
	fwrite(temp,4,1,file);
	uint32ToBytes((uint32_t)self->app->editor.sectory,temp);
	fwrite(temp,4,1,file);

	fclose(file);
}
