// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_MENU_H
#define SRC_MENU_H

#include <SDL2/SDL.h>
#include <stdbool.h>

#define MENU_MAX_ITEM_COUNT 35
#define MENU_ITEM_HEIGHT 20

typedef struct App App;

typedef enum MenuItemType{
	MENU_ITEM_TYPE_LABEL,MENU_ITEM_TYPE_SEPARATOR,MENU_ITEM_TYPE_HEADER
} MenuItemType;

typedef struct MenuItem{
	char* text;
	SDL_Texture* textTexture;
	void(*labelFunc)(void*);
	void* labelFuncArg;
	MenuItemType type;
} MenuItem;

typedef enum MenuState{
	MENU_STATE_HIDDEN,MENU_STATE_OPENING,MENU_STATE_HIDDING,MENU_STATE_OPENED
} MenuState;

typedef struct Menu{
	MenuItem items[MENU_MAX_ITEM_COUNT];
	SDL_Renderer* renderer;
	int itemCount;
	float xpos;
	float ypos;
	float width;
	float height;
	int hoveredItem;
	MenuState state;
	App* app;
} Menu;
void Menu_init(Menu* self,SDL_Renderer* renderer,App* app);
void Menu_delete(Menu* self);
void Menu_addLabelItem(Menu* self,char* text,void(*func)(void*),void* arg);
void Menu_addSeparatorItem(Menu* self);
void Menu_addHeaderItem(Menu* self,char* text);
void Menu_updateItemText(Menu* self,unsigned index,char* newText);
void Menu_updateMenuYPos(Menu* self);
void Menu_open(Menu* self);
void Menu_close(Menu* self);
void Menu_closeWithoutAnimation(Menu* self);
void Menu_draw(Menu* self);
void Menu_update(Menu* self,SDL_Event* event);
bool Menu_isMenuOpened();

#endif
