// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "menu.h"

#include "app.h"
#include <SDL2/SDL_video.h>

static bool Menu_opened=false;

void Menu_init(Menu* self,SDL_Renderer* renderer,App* app){
	self->renderer=renderer;
	self->itemCount=0;
	self->xpos=0;
	self->ypos=0;
	self->width=0;
	self->height=0;
	self->hoveredItem=0;
	self->state=MENU_STATE_HIDDEN;
	self->app=app;
}
void Menu_delete(Menu* self){
	for(unsigned i=0; i<self->itemCount; i++){
		SDL_DestroyTexture(self->items[i].textTexture);
	}
}
void Menu_addLabelItem(Menu* self,char* text,void(*func)(void*),void* arg){
	if(self->itemCount>=MENU_MAX_ITEM_COUNT)
		return;

	SDL_Surface* textSurf=TTF_RenderUTF8_Blended(self->app->font,text,(SDL_Color){255,255,255,255});

	MenuItem* item=&self->items[self->itemCount++];
	item->text=text;
	item->textTexture=SDL_CreateTextureFromSurface(self->renderer,textSurf);
	SDL_SetTextureColorMod(item->textTexture,200,200,200);
	item->labelFunc=func;
	item->labelFuncArg=arg;
	item->type=MENU_ITEM_TYPE_LABEL;
	if((unsigned)textSurf->w>self->width)
		self->width=textSurf->w+10;
	self->height+=MENU_ITEM_HEIGHT;
	self->ypos=self->app->logicalWindowSize.y/2.0f;

	SDL_FreeSurface(textSurf);
}
void Menu_addSeparatorItem(Menu* self){
	if(self->itemCount>=MENU_MAX_ITEM_COUNT)
		return;

	self->items[self->itemCount++].type=MENU_ITEM_TYPE_SEPARATOR;
	self->height+=MENU_ITEM_HEIGHT;
	self->ypos=self->app->logicalWindowSize.y/2.0f;
}
void Menu_addHeaderItem(Menu* self,char* text){
	if(self->itemCount>=MENU_MAX_ITEM_COUNT)
		return;

	SDL_Surface* textSurf=TTF_RenderUTF8_Blended(self->app->font,text,(SDL_Color){102,204,0,255});

	MenuItem* item=&self->items[self->itemCount++];
	item->type=MENU_ITEM_TYPE_HEADER;
	item->text=text;
	item->textTexture=SDL_CreateTextureFromSurface(self->renderer,textSurf);
	if((unsigned)textSurf->w>self->width)
		self->width=textSurf->w+10;
	self->height+=MENU_ITEM_HEIGHT;
	self->ypos=(190.0f-self->height)/2;

	SDL_FreeSurface(textSurf);
}
void Menu_updateItemText(Menu* self,unsigned index,char* newText){
	if(index>self->itemCount)
		return;

	MenuItem* item=&self->items[index];

	if(item->type!=MENU_ITEM_TYPE_LABEL && item->type!=MENU_ITEM_TYPE_HEADER)
		return;

	SDL_DestroyTexture(item->textTexture);

	SDL_Color textColor;
	textColor.a=255;
	switch(item->type){
	case MENU_ITEM_TYPE_LABEL:
		textColor.r=255;
		textColor.g=255;
		textColor.b=255;
		break;
	case MENU_ITEM_TYPE_HEADER:
		textColor.r=102;
		textColor.g=204;
		textColor.b=0;
		break;
	default:
		break;
	}

	SDL_Surface* textSurf=TTF_RenderUTF8_Blended(self->app->font,newText,textColor);
	if((unsigned)textSurf->w>self->width)
		self->width=textSurf->w+10;
	item->text=newText;
	item->textTexture=SDL_CreateTextureFromSurface(self->renderer,textSurf);

	SDL_FreeSurface(textSurf);
}
void Menu_updateMenuYPos(Menu *self){
	if(self->height>self->app->logicalWindowSize.y){
		// Menu scrolling code was taken and adapted from https://github.com/SuperTux/supertux/blob/1e63830572d35702826a03336ea281cb7bc9f6a6/src/gui/menu.cpp
		const float scrollRange=(self->height-self->app->logicalWindowSize.y)/2.0f;
		const float scrollPos=(((float)self->hoveredItem/(self->itemCount-1))-0.5f)*2.0f;
		self->ypos=self->app->logicalWindowSize.y/2.0f-scrollRange*scrollPos;
	}
}
void Menu_open(Menu* self){
	if(self->state!=MENU_STATE_HIDDEN || Menu_opened)
		return;

	self->state=MENU_STATE_OPENING;
	self->xpos=-self->width;
	Menu_opened=true;
}
void Menu_close(Menu* self){
	if(self->state!=MENU_STATE_OPENED)
		return;

	self->state=MENU_STATE_HIDDING;
}
void Menu_closeWithoutAnimation(Menu *self){
	if(self->state!=MENU_STATE_OPENED)
		return;

	self->state=MENU_STATE_HIDDEN;
	Menu_opened=false;
}
static void Menu_drawLabelItem(Menu* self,unsigned index){
	SDL_Rect rect;

	int width,height;
	SDL_QueryTexture(self->items[index].textTexture,0,0,&width,&height);

	rect.x=self->xpos+(self->width-width)/2;
	rect.y=self->ypos-self->height/2+index*MENU_ITEM_HEIGHT;
	rect.w=width;
	rect.h=height;

	if(index==self->hoveredItem || self->items[index].type==MENU_ITEM_TYPE_HEADER)
		SDL_SetTextureColorMod(self->items[index].textTexture,255,255,255);
	else
		SDL_SetTextureColorMod(self->items[index].textTexture,200,200,200);

	SDL_RenderCopy(self->renderer,self->items[index].textTexture,0,&rect);
}
void Menu_draw(Menu* self){
	if(self->state==MENU_STATE_HIDDEN)
		return;

	switch(self->state){
	case MENU_STATE_HIDDEN:
		break;
	case MENU_STATE_OPENING:{
		self->xpos+=150.0f*self->app->deltaTime;
		if(self->xpos>=(340.0f-self->width)/2){
			self->state=MENU_STATE_OPENED;
			self->xpos=(340.0f-self->width)/2;
		}
		break;
	}
	case MENU_STATE_HIDDING:{
		self->xpos-=150.0f*self->app->deltaTime;
		if(self->xpos<=-(int)self->width){
			self->state=MENU_STATE_HIDDEN;
			Menu_opened=false;
		}
		break;
	}
	case MENU_STATE_OPENED:
		break;
	}

	SDL_Rect rect;
	rect.x=self->xpos;
	rect.y=self->ypos-self->height/2;
	rect.w=self->width;
	rect.h=self->height;
	SDL_SetRenderDrawBlendMode(self->renderer,SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(self->renderer,40,40,40,100);
	SDL_RenderFillRect(self->renderer,&rect);

	for(unsigned i=0; i<self->itemCount; i++){
		switch(self->items[i].type){
		case MENU_ITEM_TYPE_LABEL:
			Menu_drawLabelItem(self,i);
			break;
		case MENU_ITEM_TYPE_HEADER:
			Menu_drawLabelItem(self,i);
			break;
		default:
			break;
		}
	}
}
void Menu_update(Menu* self,SDL_Event* event){
	if(self->state!=MENU_STATE_OPENED)
		return;

	if(event->type==SDL_MOUSEMOTION){
		if(self->app->logicalMousePos.x>=self->xpos && self->app->logicalMousePos.y<=self->xpos+self->width && self->app->logicalMousePos.y>=self->ypos-self->height/2.0f && self->app->logicalMousePos.y<=self->ypos+self->height/2.0f)
			self->hoveredItem=(self->app->logicalMousePos.y-(self->ypos-self->height/2.0f))/MENU_ITEM_HEIGHT;
		Menu_updateMenuYPos(self);
	}
	else if(event->type==SDL_KEYDOWN){
		if(event->key.keysym.sym==SDLK_DOWN && self->hoveredItem<self->itemCount-1){
			do{
				self->hoveredItem++;
			}
			while(self->items[self->hoveredItem].type==MENU_ITEM_TYPE_SEPARATOR || self->items[self->hoveredItem].type==MENU_ITEM_TYPE_HEADER);
			Menu_updateMenuYPos(self);
		}
		else if(event->key.keysym.sym==SDLK_UP && self->hoveredItem>0){
			do{
				self->hoveredItem--;
			}
			while(self->items[self->hoveredItem].type==MENU_ITEM_TYPE_SEPARATOR || self->items[self->hoveredItem].type==MENU_ITEM_TYPE_HEADER);
			Menu_updateMenuYPos(self);
		}
		else if(event->key.keysym.sym==SDLK_RETURN){
			if(self->items[self->hoveredItem].type==MENU_ITEM_TYPE_LABEL)
				self->items[self->hoveredItem].labelFunc(self->items[self->hoveredItem].labelFuncArg);
		}
		else if(event->key.keysym.sym==SDLK_ESCAPE)
			Menu_close(self);
	}
	else if(event->type==SDL_MOUSEBUTTONDOWN && event->button.button==SDL_BUTTON_LEFT){
		if(self->app->logicalMousePos.x>=self->xpos && self->app->logicalMousePos.y<=self->xpos+self->width && self->app->logicalMousePos.y>=self->ypos-self->height/2.0f && self->app->logicalMousePos.y<=self->ypos+self->height/2.0f && self->items[self->hoveredItem].type==MENU_ITEM_TYPE_LABEL)
			self->items[self->hoveredItem].labelFunc(self->items[self->hoveredItem].labelFuncArg);
	}
}
bool Menu_isMenuOpened(){
	return Menu_opened;
}
