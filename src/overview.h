// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_OVERVIEW_H
#define SRC_OVERVIEW_H

#include <SDL2/SDL.h>
#include <stdbool.h>

#include "menu.h"

typedef struct App App;

typedef struct Overview{
	App* app;
	unsigned sectorCount;
	SDL_Texture** sectorPreviews;
	SDL_Point* sectorPositions;
	SDL_Point offset;
	SDL_Point prevMousePosition;
	SDL_Point selectedSector;
	float scale;
	bool rightButtonDown;
	bool movingSector;
	unsigned movedSectorIndex;
	bool copyingSector;
	Menu existingSectorMenu;
	Menu deleteSectorConfirmMenu;
	SDL_Surface* tilesetSurf;
} Overview;
void Overview_init(Overview* self,App* app);
void Overview_delete(Overview* self);
void Overview_reload(Overview* self);
void Overview_draw(Overview* self);
void Overview_update(Overview* self,SDL_Event* event);
SDL_Texture* Overview_createSectorPreview(Overview* self,char* sectorData);
void Overview_updateSectorPreview(Overview* self,int x,int y,char* sectorData);
void Overview_deleteSectorPreview(Overview* self,int x,int y);
bool Overview_isMouseOnSector(Overview* self);

#endif