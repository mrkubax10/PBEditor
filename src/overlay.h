// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_OVERLAY_H
#define SRC_OVERLAY_H

#include <stdbool.h>
#include <SDL2/SDL.h>

#include "menu.h"

typedef struct Editor Editor;
typedef struct App App;

typedef struct Overlay{
	SDL_Renderer* renderer;
	SDL_Texture* rubberTexture;
	SDL_Texture* musicTexture;
	SDL_Texture* backgroundTexture;
	SDL_Texture* startFromHereTexture;
	Editor* editor;
	App* app;
	int overlayOffset;
	unsigned short selectedTile;
	bool mouseDown;
	Menu musicMenu;
	Menu backgroundMenu;
	Menu startFromHereConfirmMenu;
} Overlay;
void Overlay_init(Overlay* self,SDL_Renderer* renderer,Editor* editor,App* app);
void Overlay_delete(Overlay* self);
void Overlay_draw(Overlay* self);
void Overlay_update(Overlay* self,SDL_Event* event);
void Overlay_startFromHere(Overlay* self);


#endif
