// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_DATA_H
#define SRC_DATA_H

#include <SDL2/SDL.h>

char* Data_getFile(const char* filename,unsigned* size);
SDL_RWops* Data_getFileRW(const char* filename,char** outData);
void Data_addFile(const char* filename,char* data,uint32_t size);
void Data_updateFile(const char* filename,char* newData,uint32_t size);
char** Data_findFiles(const char* query,unsigned* size);
void Data_deleteFile(const char* filename);
void Data_moveFile(const char* filename,const char* newFilename);
void Data_swapFiles(const char* filename,const char* filename2);
void Data_copyFile(const char* filename,const char* targetFilename);
void Data_delete();

#endif
