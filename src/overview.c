// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "overview.h"

#include <SDL2/SDL_image.h>

#include "app.h"
#include "data.h"
#include "editor.h"
#include "menu.h"

static int Overview_mapSectorPositionToIndex(Overview* self,int x,int y){
	int index=-1;
	for(unsigned i=0; i<self->sectorCount; i++){
		if(self->sectorPositions[i].x==x && self->sectorPositions[i].y==y){
			index=(int)i;
			break;
		}
	}
	return index;
}

static void ExistingSectorMenu_editSector(void* arg){
	Overview* overview=(Overview*)arg;
	Editor_openSector(&overview->app->editor,overview->selectedSector.x,overview->selectedSector.y);
	overview->app->editor.overlay.mouseDown=false;
	overview->app->state=APP_STATE_EDITOR;
	Menu_closeWithoutAnimation(&overview->existingSectorMenu);
}
static void ExistingSectorMenu_moveSector(void* arg){
	Overview* overview=(Overview*)arg;
	overview->movingSector=true;
	overview->movedSectorIndex=Overview_mapSectorPositionToIndex(overview,overview->selectedSector.x,overview->selectedSector.y);
	Menu_close(&overview->existingSectorMenu);
}
static void ExistingSectorMenu_copySector(void* arg){
	Overview* overview=(Overview*)arg;
	overview->copyingSector=true;
	overview->movedSectorIndex=Overview_mapSectorPositionToIndex(overview,overview->selectedSector.x,overview->selectedSector.y);
	Menu_close(&overview->existingSectorMenu);
}
static void ExistingSectorMenu_deleteSector(void* arg){
	Overview* overview=(Overview*)arg;
	Menu_closeWithoutAnimation(&overview->existingSectorMenu);
	Menu_open(&overview->deleteSectorConfirmMenu);
}
static void DeleteSectorConfirmMenu_confirm(void* arg){
	Overview* overview=(Overview*)arg;
	char filename[512];
	snprintf(filename,512,"ASSETS/POZIOMY/%d,%d.poziom",overview->selectedSector.x,overview->selectedSector.y);
	Data_deleteFile(filename);
	Overview_deleteSectorPreview(overview,overview->selectedSector.x,overview->selectedSector.y);
	Menu_close(&overview->deleteSectorConfirmMenu);
}
static void DeleteSectorConfirmMenu_cancel(void* arg){
	Overview* overview=(Overview*)arg;
	Menu_close(&overview->deleteSectorConfirmMenu);
}

void Overview_init(Overview* self,App* app){
	self->app=app;
	self->sectorCount=0;
	self->sectorPreviews=0;
	self->scale=0.1f;
	self->offset.x=0;
	self->offset.y=0;
	self->prevMousePosition.x=self->app->logicalMousePos.x;
	self->prevMousePosition.y=self->app->logicalMousePos.y;
	self->selectedSector.x=0;
	self->selectedSector.y=0;
	self->rightButtonDown=false;
	self->movingSector=false;
	self->copyingSector=false;
	self->movedSectorIndex=0;
	self->tilesetSurf=Editor_generateTilesetSurface(&self->app->editor);

	Menu_init(&self->existingSectorMenu,self->app->renderer,self->app);
	Menu_addHeaderItem(&self->existingSectorMenu," ");
	Menu_addSeparatorItem(&self->existingSectorMenu);
	Menu_addLabelItem(&self->existingSectorMenu,"Edit sector",ExistingSectorMenu_editSector,(void*)self);
	Menu_addLabelItem(&self->existingSectorMenu,"Move sector",ExistingSectorMenu_moveSector,(void*)self);
	Menu_addLabelItem(&self->existingSectorMenu,"Copy sector",ExistingSectorMenu_copySector,(void*)self);
	Menu_addLabelItem(&self->existingSectorMenu,"Delete sector",ExistingSectorMenu_deleteSector,(void*)self);

	Menu_init(&self->deleteSectorConfirmMenu,self->app->renderer,self->app);
	Menu_addHeaderItem(&self->deleteSectorConfirmMenu,"This action will delete this sector. Are you sure?");
	Menu_addSeparatorItem(&self->deleteSectorConfirmMenu);
	Menu_addLabelItem(&self->deleteSectorConfirmMenu,"Yes",DeleteSectorConfirmMenu_confirm,(void*)self);
	Menu_addLabelItem(&self->deleteSectorConfirmMenu,"No",DeleteSectorConfirmMenu_cancel,(void*)self);

	Overview_reload(self);
}
void Overview_delete(Overview* self){
	for(unsigned i=0; i<self->sectorCount; i++){
		SDL_DestroyTexture(self->sectorPreviews[i]);
	}

	SDL_FreeSurface(self->tilesetSurf);
	free(self->sectorPreviews);
	free(self->sectorPositions);
}
void Overview_reload(Overview* self){
	if(self->sectorPreviews)
		Overview_delete(self);

	char** sectorFilenames=Data_findFiles("ASSETS/POZIOMY/",&self->sectorCount);
	self->sectorPreviews=malloc(self->sectorCount*sizeof(SDL_Texture*));
	self->sectorPositions=malloc(self->sectorCount*sizeof(SDL_Point));

	unsigned size;
	for(unsigned i=0; i<self->sectorCount; i++){
		char* sectorData=Data_getFile(sectorFilenames[i],&size);
	
		if(size<SECTOR_WIDTH*SECTOR_HEIGHT*sizeof(unsigned short)){
			free(sectorData);
			continue;
		}

		self->sectorPreviews[i]=Overview_createSectorPreview(self,sectorData);
		free(sectorData);

		sscanf(sectorFilenames[i],"ASSETS/POZIOMY/%d,%d.poziom",&self->sectorPositions[i].x,&self->sectorPositions[i].y);
	}

	free(sectorFilenames);
}
void Overview_draw(Overview* self){
	SDL_FRect rect;
	float scaledSectorWidth=SECTOR_WIDTH*TILE_SIZE*self->scale;
	float scaledSectorHeight=SECTOR_HEIGHT*TILE_SIZE*self->scale;
	rect.w=scaledSectorWidth;
	rect.h=scaledSectorHeight;

	for(unsigned i=0; i<self->sectorCount; i++){
		rect.x=self->sectorPositions[i].x*SECTOR_WIDTH*TILE_SIZE*self->scale-self->offset.x;
		rect.y=self->sectorPositions[i].y*SECTOR_HEIGHT*TILE_SIZE*self->scale-self->offset.y;
		if(!self->movingSector || self->sectorPositions[i].x!=self->sectorPositions[self->movedSectorIndex].x || self->sectorPositions[i].y!=self->sectorPositions[self->movedSectorIndex].y)
			SDL_RenderCopyF(self->app->renderer,self->sectorPreviews[i],0,&rect);
	}

	if(!Menu_isMenuOpened()){
		rect.x=(self->app->logicalMousePos.x+self->offset.x)/(int)scaledSectorWidth*(float)scaledSectorWidth-self->offset.x;
		rect.y=(self->app->logicalMousePos.y+self->offset.y)/(int)scaledSectorHeight*(float)scaledSectorHeight-self->offset.y;
		if(self->movingSector || self->copyingSector)
			SDL_RenderCopyF(self->app->renderer,self->sectorPreviews[self->movedSectorIndex],0,&rect);
		SDL_SetRenderDrawColor(self->app->renderer,255,255,255,255);
		SDL_RenderDrawRectF(self->app->renderer,&rect);

		if(self->rightButtonDown){
			self->offset.x-=self->app->logicalMousePos.x-self->prevMousePosition.x;
			self->offset.y-=self->app->logicalMousePos.y-self->prevMousePosition.y;
			self->prevMousePosition.x=self->app->logicalMousePos.x;
			self->prevMousePosition.y=self->app->logicalMousePos.y;
		}
	}

	Menu_draw(&self->existingSectorMenu);
	Menu_draw(&self->deleteSectorConfirmMenu);
}
void Overview_update(Overview* self,SDL_Event* event){
	if(!Menu_isMenuOpened()){
		if(event->type==SDL_MOUSEWHEEL){
			float prevScale=self->scale;
			self->scale+=event->wheel.y*0.01f;
			if(self->scale>4)
				self->scale=4;
			else if(self->scale<0.1f)
				self->scale=0.1f;
			// TODO: Center camera while scaling
			if(prevScale>self->scale){
				self->offset.x+=self->app->logicalMousePos.x/2.0f*self->scale;
				self->offset.y+=self->app->logicalMousePos.y/2.0f*self->scale;
			}
			else if(prevScale<self->scale){
				self->offset.x-=self->app->logicalMousePos.x/2.0f*self->scale;
				self->offset.y-=self->app->logicalMousePos.y/2.0f*self->scale;
			}
		}
		else if(event->type==SDL_MOUSEBUTTONDOWN){
			if(event->button.button==SDL_BUTTON_LEFT){
				self->selectedSector.x=(self->app->logicalMousePos.x+self->offset.x)/(SECTOR_WIDTH*TILE_SIZE*self->scale);
				self->selectedSector.y=(self->app->logicalMousePos.y+self->offset.y)/(SECTOR_HEIGHT*TILE_SIZE*self->scale);
				if(self->movingSector){
					if(Overview_isMouseOnSector(self)){
						char filename[512];
						snprintf(filename,512,"ASSETS/POZIOMY/%d,%d.poziom",self->selectedSector.x,self->selectedSector.y);
						char filename2[512];
						snprintf(filename2,512,"ASSETS/POZIOMY/%d,%d.poziom",self->sectorPositions[self->movedSectorIndex].x,self->sectorPositions[self->movedSectorIndex].y);
						Data_swapFiles(filename,filename2);

						int index=Overview_mapSectorPositionToIndex(self,self->selectedSector.x,self->selectedSector.y);
						self->sectorPositions[index].x=self->sectorPositions[self->movedSectorIndex].x;
						self->sectorPositions[index].y=self->sectorPositions[self->movedSectorIndex].y;
						self->sectorPositions[self->movedSectorIndex].x=self->selectedSector.x;
						self->sectorPositions[self->movedSectorIndex].y=self->selectedSector.y;
					}
					else{
						char filename[512];
						snprintf(filename,512,"ASSETS/POZIOMY/%d,%d.poziom",self->sectorPositions[self->movedSectorIndex].x,self->sectorPositions[self->movedSectorIndex].y);
						char newFilename[512];
						snprintf(newFilename,512,"ASSETS/POZIOMY/%d,%d.poziom",self->selectedSector.x,self->selectedSector.y);
						Data_moveFile(filename,newFilename);
						self->sectorPositions[self->movedSectorIndex].x=self->selectedSector.x;
						self->sectorPositions[self->movedSectorIndex].y=self->selectedSector.y;
					}
					self->movingSector=false;
				}
				else if(self->copyingSector){
					if(!Overview_isMouseOnSector(self)){
						char filename[512];
						snprintf(filename,512,"ASSETS/POZIOMY/%d,%d.poziom",self->sectorPositions[self->movedSectorIndex].x,self->sectorPositions[self->movedSectorIndex].y);
						char targetFilename[512];
						snprintf(targetFilename,512,"ASSETS/POZIOMY/%d,%d.poziom",self->selectedSector.x,self->selectedSector.y);

						Data_copyFile(filename,targetFilename);

						self->sectorCount++;
						self->sectorPositions=realloc(self->sectorPositions,self->sectorCount*sizeof(SDL_Point));
						self->sectorPositions[self->sectorCount-1].x=self->selectedSector.x;
						self->sectorPositions[self->sectorCount-1].y=self->selectedSector.y;
						self->sectorPreviews=realloc(self->sectorPreviews,self->sectorCount*sizeof(SDL_Texture*));
						unsigned size;
						char* sectorData=Data_getFile(targetFilename,&size);
						self->sectorPreviews[self->sectorCount-1]=Overview_createSectorPreview(self,sectorData);
						free(sectorData);

						if(!(SDL_GetModState() & KMOD_CTRL))
							self->copyingSector=false;
					}
				}
				else{
					if(Overview_isMouseOnSector(self)){
						if(!Menu_isMenuOpened()){
							char title[512];
							snprintf(title,512,"Sector %d,%d",self->selectedSector.x,self->selectedSector.y);
							Menu_updateItemText(&self->existingSectorMenu,0,title);
							Menu_open(&self->existingSectorMenu);
						}
					}
					else{
						Editor_openSector(&self->app->editor,self->selectedSector.x,self->selectedSector.y);
						self->app->state=APP_STATE_EDITOR;
					}
				}
			}
			else if(event->button.button==SDL_BUTTON_RIGHT){
				self->prevMousePosition.x=self->app->logicalMousePos.x;
				self->prevMousePosition.y=self->app->logicalMousePos.y;
				self->rightButtonDown=true;
			}
		}
		else if(event->type==SDL_MOUSEBUTTONUP){
			if(event->button.button==SDL_BUTTON_RIGHT)
				self->rightButtonDown=false;
		}
		else if(event->type==SDL_KEYDOWN && event->key.keysym.sym==SDLK_ESCAPE){
			if(self->movingSector)
				self->movingSector=false;
			else if(self->copyingSector)
				self->copyingSector=false;
			else{
				self->app->editor.overlay.mouseDown=false;
				self->app->state=APP_STATE_EDITOR;
			}
		}
	}

	if(!self->movingSector && !self->copyingSector){
		Menu_update(&self->existingSectorMenu,event);
		Menu_update(&self->deleteSectorConfirmMenu,event);
	}
}
SDL_Texture* Overview_createSectorPreview(Overview* self,char* sectorData){
	unsigned size=SECTOR_WIDTH*SECTOR_WIDTH*sizeof(uint16_t);
	unsigned short* sector=(unsigned short*)sectorData;

	SDL_Surface* outputSurf=SDL_CreateRGBSurface(0,SECTOR_WIDTH*TILE_SIZE,SECTOR_HEIGHT*TILE_SIZE,32,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	SDL_FillRect(outputSurf,0,SDL_MapRGB(outputSurf->format,0,0,0));

	unsigned sectorSize=SECTOR_WIDTH*SECTOR_HEIGHT;
	if(size>=sectorSize*sizeof(unsigned short)+sizeof(unsigned short)){
		// Background ID entry is present, load it
		char backgroundFilename[512];
		snprintf(backgroundFilename,512,"ASSETS/GRAFIKA/TLA/%d.png",sector[sectorSize]);
	
		char* backgroundData;
		SDL_RWops* rw=Data_getFileRW(backgroundFilename,&backgroundData);
		SDL_Surface* backgroundUnit=IMG_Load_RW(rw,0);
		SDL_Surface* background=Editor_createBackground(&self->app->editor, backgroundUnit);
		SDL_FreeSurface(backgroundUnit);

		SDL_BlitSurface(background,0,outputSurf,0);

		SDL_FreeRW(rw);
		free(backgroundData);
		SDL_FreeSurface(background);
	}

	SDL_Rect rect,rect2;
	rect.x=0;
	rect.w=TILE_SIZE;
	rect.h=TILE_SIZE;
	rect2.w=TILE_SIZE;
	rect2.h=TILE_SIZE;

	for(unsigned x=0; x<SECTOR_WIDTH; x++){
		for(unsigned y=0; y<SECTOR_HEIGHT; y++){
			rect.y=(sector[y*SECTOR_WIDTH+x]-1)*TILE_SIZE;
			rect2.x=x*TILE_SIZE;
			rect2.y=y*TILE_SIZE;
			SDL_BlitSurface(self->tilesetSurf,&rect,outputSurf,&rect2);
		}
	}

	SDL_Texture* output=SDL_CreateTextureFromSurface(self->app->renderer,outputSurf);
	SDL_FreeSurface(outputSurf);

	return output;
}
void Overview_updateSectorPreview(Overview *self,int x,int y,char* sectorData){
	int index=Overview_mapSectorPositionToIndex(self,x,y);

	if(index==-1){
		self->sectorCount++;
		self->sectorPositions=realloc(self->sectorPositions,self->sectorCount*sizeof(SDL_Point));
		self->sectorPreviews=realloc(self->sectorPreviews,self->sectorCount*sizeof(SDL_Texture*));
		self->sectorPositions[self->sectorCount-1].x=x;
		self->sectorPositions[self->sectorCount-1].y=y;
		self->sectorPreviews[self->sectorCount-1]=Overview_createSectorPreview(self,sectorData);
		return;
	}

	SDL_DestroyTexture(self->sectorPreviews[index]);
	self->sectorPreviews[index]=Overview_createSectorPreview(self,sectorData);
}
void Overview_deleteSectorPreview(Overview *self,int x,int y){
	int index=Overview_mapSectorPositionToIndex(self,x,y);
	if(index==-1)
		return;

	SDL_DestroyTexture(self->sectorPreviews[index]);
	if(index<self->sectorCount-1){
		for(unsigned i=(unsigned)index+1; i<self->sectorCount; i++){
			self->sectorPositions[i-1]=self->sectorPositions[i];
			self->sectorPreviews[i-1]=self->sectorPreviews[i];
		}
	}
	self->sectorCount--;
	self->sectorPositions=realloc(self->sectorPositions,self->sectorCount*sizeof(SDL_Point));
	self->sectorPreviews=realloc(self->sectorPreviews,self->sectorCount*sizeof(SDL_Texture*));
}
bool Overview_isMouseOnSector(Overview* self){
	float scaledSectorWidth=SECTOR_WIDTH*TILE_SIZE*self->scale;
	float scaledSectorHeight=SECTOR_HEIGHT*TILE_SIZE*self->scale;

	for(unsigned i=0; i<self->sectorCount; i++){
		SDL_Point translatedSectorPosition={(self->sectorPositions[i].x*SECTOR_WIDTH*TILE_SIZE)*self->scale-self->offset.x,(self->sectorPositions[i].y*SECTOR_HEIGHT*TILE_SIZE)*self->scale-self->offset.y};
		if(self->app->logicalMousePos.x>=translatedSectorPosition.x && self->app->logicalMousePos.x<=translatedSectorPosition.x+scaledSectorWidth && self->app->logicalMousePos.y>=translatedSectorPosition.y && self->app->logicalMousePos.y<=translatedSectorPosition.y+scaledSectorHeight){
			return true;
		}
	}

	return false;
}
