// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_APP_H
#define SRC_APP_H

#include <stdbool.h>
#include <SDL2/SDL_ttf.h>

#include "editor.h"
#include "overview.h"

typedef enum AppState{
	APP_STATE_EDITOR,APP_STATE_OVERVIEW
} AppState;

typedef struct App{
	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_Event event;
	float deltaTime;
	Uint32 frameTimeStart;
	Editor editor;
	bool running;
	SDL_Point logicalMousePos;
	SDL_Point logicalWindowSize;
	TTF_Font* font;
	TTF_Font* fontBig;
	SDL_RWops* fontRW;
	char* fontData;
	AppState state;
	Overview overview;
} App;
void App_init(App* self);
void App_delete(App* self);
void App_loop(App* self);

#endif
