// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "data.h"

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "binary_utils.h"

typedef struct ArchiveFile{
	char* filename;
	uint32_t filenameLength;
	uint32_t size;
	uint32_t offset;
	unsigned headerOffset;
} ArchiveFile;
static void ArchiveFile_delete(ArchiveFile* self){
	if(self->filename)
		free(self->filename);
}

static void decode(char* data,unsigned length){
	for(unsigned i=0; i<length; i++){
		data[i]-=i;
	}
}
static void encode(char* data,unsigned length){
	for(unsigned i=length-1; i>0; i--){
		data[i]+=i;
	}
}

static void readFileData(FILE* gamedata,ArchiveFile* in){
	char temp[4];
	fread(temp,4,1,gamedata);
	in->size=bytesToUint32(temp);
	fread(temp,4,1,gamedata);
	in->filenameLength=bytesToUint32(temp);
	fread(temp,4,1,gamedata);
	in->offset=bytesToUint32(temp);
	in->filename=malloc(in->filenameLength+1);
	fread(in->filename,in->filenameLength,1,gamedata);
	decode(in->filename,in->filenameLength);
	in->filename[in->filenameLength]=0;
}
static ArchiveFile* getFileList(FILE* gamedata,unsigned fileCount,unsigned* fileListLength){
	ArchiveFile* files=malloc(fileCount*sizeof(ArchiveFile));
	*fileListLength=0;
	for(unsigned i=0; i<fileCount; i++){
		readFileData(gamedata,&files[i]);
		files[i].headerOffset=(*fileListLength)+4;
		(*fileListLength)+=files[i].filenameLength+12;
	}
	return files;
}

static ArchiveFile* dataFiles=0;
static FILE* dataFile=0;
static uint32_t dataFileCount=0;
static unsigned dataFileListLength=0;
static long dataFileSize=0;

static ArchiveFile* Data_lookupFile(const char* filename,unsigned* index){
	if(!dataFiles)
		dataFiles=getFileList(dataFile,dataFileCount,&dataFileListLength);

	for(unsigned i=0; i<dataFileCount; i++){
		if(strcmp(filename,dataFiles[i].filename)==0){
			if(index)
				*index=i;
			return &dataFiles[i];
		}
	}

	return 0;
}
char* Data_getFile(const char* filename,unsigned* size){
	if(!dataFile){
		dataFile=fopen("gamedata","rb+");
		if(!dataFile){
			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"PBEditor","Failed to open gamedata file",0);
			exit(1);
		}

		fseek(dataFile,0,SEEK_END);
		dataFileSize=ftell(dataFile);
		rewind(dataFile);

		char temp[4];
		fread(temp,4,1,dataFile);
		dataFileCount=bytesToUint32(temp);
	}
	ArchiveFile* file=Data_lookupFile(filename,0);
	if(!file)
		return 0;

	char* fileData=malloc(file->size);
	fseek(dataFile,file->offset+dataFileListLength+4,SEEK_SET);
	fread(fileData,file->size,1,dataFile);
	decode(fileData,file->size);
	*size=file->size;

	return fileData;
}
SDL_RWops* Data_getFileRW(const char* filename,char** outData){
	unsigned size;
	char* data=Data_getFile(filename,&size);
	if(!data){
		*outData=0;
		return 0;
	}
	SDL_RWops* rw=SDL_RWFromMem(data,size);
	*outData=data;
	return rw;
}
void Data_addFile(const char *filename,char *data,uint32_t size){
	if(Data_lookupFile(filename,0))
		return;

	unsigned fileDataSize=dataFileSize-(dataFileListLength+sizeof(uint32_t));
	char* fileData=malloc(fileDataSize);
	fseek(dataFile,dataFileListLength+sizeof(uint32_t),SEEK_SET);
	fread(fileData,fileDataSize,1,dataFile);
	fseek(dataFile,dataFileListLength+sizeof(uint32_t),SEEK_SET);

	char temp[4];
	uint32ToBytes(size,temp);
	fwrite(temp,4,1,dataFile);

	uint32_t filenameLength=(uint32_t)strlen(filename);

	uint32ToBytes(filenameLength,temp);
	fwrite(temp,4,1,dataFile);

	uint32_t newFileOffset=fileDataSize;
	uint32ToBytes(newFileOffset,temp);
	fwrite(temp,4,1,dataFile);

	char filenameCopy[filenameLength];
	memcpy(filenameCopy,filename,filenameLength); // Use memcpy instead of strcpy to avoid copying null terminator
	encode(filenameCopy,filenameLength);
	fwrite(filenameCopy,filenameLength,1,dataFile);

	fwrite(fileData,fileDataSize,1,dataFile);
	free(fileData);

	encode(data,size);
	fwrite(data,size,1,dataFile);

	dataFileCount++;
	rewind(dataFile);
	uint32ToBytes(dataFileCount,temp);
	fwrite(temp,4,1,dataFile);

	dataFiles=realloc(dataFiles,dataFileCount*sizeof(ArchiveFile));
	dataFiles[dataFileCount-1].filename=malloc(filenameLength+1);
	strcpy(dataFiles[dataFileCount-1].filename,filename);
	dataFiles[dataFileCount-1].filenameLength=filenameLength;
	dataFiles[dataFileCount-1].headerOffset=dataFileListLength+sizeof(uint32_t);
	dataFiles[dataFileCount-1].size=size;
	dataFiles[dataFileCount-1].offset=newFileOffset;
	dataFileListLength+=filenameLength+12;
	dataFileSize+=filenameLength+12+size;
}
void Data_updateFile(const char* filename,char* newData,uint32_t size){
	unsigned index;
	ArchiveFile* file=Data_lookupFile(filename,&index);
	if(!file)
		Data_addFile(filename,newData,size);
	else if(size==file->size){
		encode(newData,size);
		fseek(dataFile,file->offset+dataFileListLength+sizeof(uint32_t),SEEK_SET);
		fwrite(newData,size,1,dataFile);
	}
	else{
		int32_t sizeDiff=size-file->size;

		uint32_t remainingSize=dataFileSize-(file->offset+file->size+dataFileListLength+sizeof(uint32_t));
		char* remaining=malloc(remainingSize);
		fseek(dataFile,file->offset+dataFileListLength+sizeof(uint32_t)+file->size,SEEK_SET);
		fread(remaining,remainingSize,1,dataFile);

		encode(newData,size);
		fseek(dataFile,file->offset+dataFileListLength+sizeof(uint32_t),SEEK_SET);
		fwrite(newData,size,1,dataFile);

		fwrite(remaining,remainingSize,1,dataFile);
		free(remaining);

		if(index<dataFileCount-1){
			for(unsigned i=index+1; i<dataFileCount; i++){
				dataFiles[i].offset+=sizeDiff;
				fseek(dataFile,dataFiles[i].headerOffset+8,SEEK_SET);
				char temp[4];
				uint32ToBytes(dataFiles[i].offset,temp);
				fwrite(temp,4,1,dataFile);
			}
		}
		dataFileSize+=sizeDiff;
		file->size=size;
	}
}
char** Data_findFiles(const char* query,unsigned* size){
	char** output=0;
	*size=0;
	unsigned queryLength=strlen(query);

	for(unsigned i=0; i<dataFileCount; i++){
		if(queryLength>dataFiles[i].filenameLength)
			continue;

		if(strncmp(query,dataFiles[i].filename,queryLength)==0){
			(*size)++;
			output=realloc(output,(*size)*sizeof(char*));
			output[(*size)-1]=dataFiles[i].filename;
		}
	}

	return output;
}
void Data_deleteFile(const char* filename){
	unsigned index;
	ArchiveFile* file=Data_lookupFile(filename,&index);
	if(!file)
		return;

	// Delete file from file list
	uint32_t remainingSize=dataFileSize-(file->headerOffset+12+file->filenameLength);
	char* remaining=malloc(remainingSize);
	fseek(dataFile,file->headerOffset+12+file->filenameLength,SEEK_SET);
	fread(remaining,remainingSize,1,dataFile);
	fseek(dataFile,file->headerOffset,SEEK_SET);
	fwrite(remaining,remainingSize,1,dataFile);
	free(remaining);
	dataFileListLength-=(12+file->filenameLength);
	dataFileSize-=(12+file->filenameLength);

	// Save file size and filename length before they get deleted
	uint32_t fileSize=file->size;
	uint32_t filenameLength=file->filenameLength;

	// Delete file from data section
	remainingSize=dataFileSize-(file->offset+file->size+dataFileListLength+sizeof(uint32_t));
	remaining=malloc(remainingSize);
	fseek(dataFile,file->offset+dataFileListLength+sizeof(uint32_t)+file->size,SEEK_SET);
	fread(remaining,remainingSize,1,dataFile);
	fseek(dataFile,file->offset+dataFileListLength+sizeof(uint32_t),SEEK_SET);
	fwrite(remaining,remainingSize,1,dataFile);
	free(remaining);

	ArchiveFile_delete(file);

	if(index<dataFileCount-1){
		for(unsigned i=index+1; i<dataFileCount; i++){
			dataFiles[i].offset-=fileSize;
			dataFiles[i].headerOffset-=(12+filenameLength);
			fseek(dataFile,dataFiles[i].headerOffset+8,SEEK_SET);
			char temp[4];
			uint32ToBytes(dataFiles[i].offset,temp);
			fwrite(temp,4,1,dataFile);
			dataFiles[i-1]=dataFiles[i];
		}
	}
	dataFileSize-=fileSize;
	dataFileCount--;
	dataFiles=realloc(dataFiles,dataFileCount*sizeof(ArchiveFile));

	// Update file count
	char temp[4];
	uint32ToBytes(dataFileCount,temp);
	rewind(dataFile);
	fwrite(temp,4,1,dataFile);
}
static void Data_moveFileInternal(ArchiveFile* file,unsigned index,const char* newFilename){
	uint32_t newFilenameLength=strlen(newFilename);

	char newFilenameCopy[newFilenameLength];
	memcpy(newFilenameCopy,newFilename,newFilenameLength);
	encode(newFilenameCopy,newFilenameLength);

	if(file->filenameLength==newFilenameLength){
		fseek(dataFile,file->headerOffset+12,SEEK_SET);
		fwrite(newFilenameCopy,newFilenameLength,1,dataFile);

		strcpy(file->filename,newFilename);
	}
	else{
		int32_t filenameSizeDiff=(int32_t)newFilenameLength-(int32_t)file->filenameLength;
		uint32_t remainingSize=dataFileSize-(file->headerOffset+12+file->filenameLength);
		char* remaining=malloc(remainingSize);
		fseek(dataFile,file->headerOffset+12+file->filenameLength,SEEK_SET);
		fread(remaining,remainingSize,1,dataFile);

		fseek(dataFile,file->headerOffset+4,SEEK_SET);
		char temp[4];
		uint32ToBytes(newFilenameLength,temp);
		fwrite(temp,4,1,dataFile);

		fseek(dataFile,file->headerOffset+12,SEEK_SET);
		fwrite(newFilenameCopy,newFilenameLength,1,dataFile);

		fwrite(remaining,remainingSize,1,dataFile);
		free(remaining);

		free(file->filename);
		file->filename=malloc(newFilenameLength+1);
		strcpy(file->filename,newFilename);
		file->filenameLength=newFilenameLength;

		if(index<dataFileCount-1){
			for(unsigned i=index+1; i<dataFileCount; i++){
				dataFiles[i].headerOffset+=filenameSizeDiff;
			}
		}

		dataFileListLength+=filenameSizeDiff;
		dataFileSize+=filenameSizeDiff;
	}
}
void Data_moveFile(const char* filename,const char* newFilename){
	unsigned index;
	ArchiveFile* file=Data_lookupFile(filename,&index);
	if(!file)
		return;

	Data_moveFileInternal(file,index,newFilename);
}
void Data_swapFiles(const char* filename,const char* filename2){
	unsigned index1;
	unsigned index2;
	ArchiveFile* file1=Data_lookupFile(filename,&index1);
	ArchiveFile* file2=Data_lookupFile(filename2,&index2);
	if(!file1 || !file2)
		return;

	Data_moveFileInternal(file1,index1,filename2);
	Data_moveFileInternal(file2,index2,filename);
}
void Data_copyFile(const char* filename,const char* targetFilename){
	if(!Data_lookupFile(filename,0))
		return;
	
	unsigned size;
	char* fileData=Data_getFile(filename,&size);
	Data_addFile(targetFilename,fileData,size);
	free(fileData);
}
void Data_delete(){
	for(unsigned i=0; i<dataFileCount; i++){
		ArchiveFile_delete(&dataFiles[i]);
	}
	free(dataFiles);

	fclose(dataFile);
}
