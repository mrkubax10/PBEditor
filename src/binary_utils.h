// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef SRC_BINARY_UTILS_H
#define SRC_BINARY_UTILS_H

#include <stdint.h>

uint16_t bytesToUint16(char* data);
void uint16ToBytes(uint16_t data,char* output);
uint32_t bytesToUint32(char* data);
void uint32ToBytes(uint32_t data,char* output);
void byteArrayToUint16Array(char* data,uint16_t* output,unsigned length);
void uint16ArrayToByteArray(uint16_t* data,char* output,unsigned length);

#endif