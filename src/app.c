// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "app.h"

#include <SDL2/SDL_image.h>

#include "data.h"
#include "build_config.h"

void App_init(App* self){
	SDL_Init(SDL_INIT_EVERYTHING);
	TTF_Init();

	self->window=SDL_CreateWindow(PBEDITOR_VERSION_STRING,SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,340,190,SDL_WINDOW_RESIZABLE);

	char* iconData;
	SDL_RWops* rw=Data_getFileRW("ASSETS/GRAFIKA/KLOCKI/189.png",&iconData);
	SDL_Surface* icon=IMG_Load_RW(rw,0);
	SDL_SetColorKey(icon,SDL_TRUE,SDL_MapRGB(icon->format,0,255,0));
	SDL_FreeRW(rw);
	SDL_SetWindowIcon(self->window,icon);
	SDL_FreeSurface(icon);
	free(iconData);

	self->renderer=SDL_CreateRenderer(self->window,0,SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
	self->logicalWindowSize.x=340;
	self->logicalWindowSize.y=190;
	SDL_RenderSetLogicalSize(self->renderer,self->logicalWindowSize.x,self->logicalWindowSize.y);

	self->running=true;
	self->logicalMousePos.x=0;
	self->logicalMousePos.y=0;

	self->fontRW=Data_getFileRW("ASSETS/font.ttf",&self->fontData);
	if(self->fontData){
		self->font=TTF_OpenFontRW(self->fontRW,0,12);
		self->fontBig=TTF_OpenFontRW(self->fontRW,0,18);
	}

	self->state=APP_STATE_EDITOR;
	self->deltaTime=0;
	self->frameTimeStart=0;
	Editor_init(&self->editor,self->renderer,self);
	Overview_init(&self->overview,self);
}
void App_delete(App* self){
	Editor_delete(&self->editor);
	Overview_delete(&self->overview);
	Data_delete();

	if(self->fontData){
		TTF_CloseFont(self->font);
		TTF_CloseFont(self->fontBig);
		SDL_FreeRW(self->fontRW);
		free(self->fontData);
	}

	SDL_DestroyRenderer(self->renderer);
	SDL_DestroyWindow(self->window);
}
void App_loop(App* self){
	while(self->running){
		self->frameTimeStart=SDL_GetTicks();

		while(SDL_PollEvent(&self->event)){
			if(self->event.type==SDL_QUIT)
				self->running=false;
			else if(self->event.type==SDL_MOUSEMOTION){
				self->logicalMousePos.x=self->event.motion.x;
				self->logicalMousePos.y=self->event.motion.y;
			}
			switch(self->state){
			case APP_STATE_EDITOR:
				Editor_update(&self->editor,&self->event);
				break;
			case APP_STATE_OVERVIEW:
				Overview_update(&self->overview,&self->event);
			}
		}

		SDL_SetRenderDrawColor(self->renderer,0,0,0,255);
		SDL_RenderClear(self->renderer);

		switch(self->state){
		case APP_STATE_EDITOR:
			Editor_draw(&self->editor);
			break;
		case APP_STATE_OVERVIEW:
			Overview_draw(&self->overview);
			break;
		}

		SDL_RenderPresent(self->renderer);
		self->deltaTime=(float)(SDL_GetTicks()-self->frameTimeStart)/100.0f;
	}
}
