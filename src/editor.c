// PBEditor - Unofficial Perypetie Boba / Bob's Adventure Editor
// Copyright (C) 2022 mrkubax10

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "editor.h"

#include <SDL2/SDL_image.h>

#include "data.h"
#include "app.h"
#include "binary_utils.h"
#include "menu.h"
#include "overview.h"

static void MainMenu_back(void* arg){
	Editor* editor=(Editor*)arg;
	Menu_close(&editor->editorMenu);
}
void MainMenu_save(void* arg){
	Editor* editor=(Editor*)arg;
	Editor_save(editor);
}
static void MainMenu_openOverview(void* arg){
	Editor* editor=(Editor*)arg;
	editor->app->state=APP_STATE_OVERVIEW;
	Menu_closeWithoutAnimation(&editor->editorMenu);
}

void Editor_init(Editor* self,SDL_Renderer* renderer,App* app){
	self->renderer=renderer;
	self->app=app;
	self->background=0;
	self->sectorBackgroundID=0;
	self->sectorMusicID=0;
	self->sectorUnknown1=0;
	self->sectorMetadata=0;
	self->sectorMetadataSize=0;
	self->sectorx=0;
	self->sectory=0;
	memset(self->sector,0,SECTOR_WIDTH*SECTOR_HEIGHT*sizeof(unsigned short));

	printf("(Log) [Editor] Generating tileset texture\n");
	SDL_Surface* tilesetSurf=Editor_generateTilesetSurface(self);
	self->tileset=SDL_CreateTextureFromSurface(self->renderer,tilesetSurf);
	SDL_FreeSurface(tilesetSurf);
	printf("(Log) [Editor] Done\n");

	Overlay_init(&self->overlay,self->renderer,self,app);

	Menu_init(&self->editorMenu,self->renderer,app);
	Menu_addLabelItem(&self->editorMenu,"Back",MainMenu_back,(void*)self);
	Menu_addLabelItem(&self->editorMenu,"Save",MainMenu_save,(void*)self);
	Menu_addSeparatorItem(&self->editorMenu);
	Menu_addLabelItem(&self->editorMenu,"Open overview",MainMenu_openOverview,(void*)self);

	Editor_openSector(self,self->sectorx,self->sectory);
}
void Editor_delete(Editor* self){
	SDL_DestroyTexture(self->tileset);
	if(self->sectorMetadata)
		free(self->sectorMetadata);
}
void Editor_update(Editor* self,SDL_Event* event){
	Overlay_update(&self->overlay,event);

	if(event->type==SDL_KEYDOWN){
		if(event->key.keysym.sym==SDLK_ESCAPE && !Menu_isMenuOpened())
			Menu_open(&self->editorMenu);
	}

	Menu_update(&self->editorMenu,event);
}
void Editor_draw(Editor* self){
	if(self->background)
		SDL_RenderCopy(self->renderer,self->background,0,0);

	SDL_Rect rect;
	rect.x=0;
	rect.w=TILE_SIZE;
	rect.h=TILE_SIZE;
	SDL_Rect rect2;
	rect2.w=TILE_SIZE;
	rect2.h=TILE_SIZE;
	for(unsigned x=0; x<SECTOR_WIDTH; x++){
		for(unsigned y=0; y<SECTOR_HEIGHT; y++){
			rect.y=(self->sector[y*SECTOR_WIDTH+x]-1)*TILE_SIZE;
			rect2.x=x*TILE_SIZE;
			rect2.y=y*TILE_SIZE;
			SDL_RenderCopy(self->renderer,self->tileset,&rect,&rect2);
		}
	}

	Overlay_draw(&self->overlay);
	Menu_draw(&self->editorMenu);
}
void Editor_openSector(Editor* self,int x,int y){
	char filename[512];
	snprintf(filename,512,"ASSETS/POZIOMY/%d,%d.poziom",x,y);
	unsigned size;
	char* sectorData=Data_getFile(filename,&size);
	unsigned sectorSize=SECTOR_WIDTH*SECTOR_HEIGHT*sizeof(uint16_t);

	self->sectorx=x;
	self->sectory=y;

	if(!sectorData){
		memset(self->sector,0,sectorSize);
		self->sectorBackgroundID=0;
		printf("(Log) [Editor] Loaded new sector %d,%d\n",x,y);
		return;
	}

	if(size<sectorSize){
		free(sectorData);
		memset(self->sector,0,sectorSize);
		self->sectorBackgroundID=0;
		return;
	}

	byteArrayToUint16Array(sectorData,self->sector,sectorSize);

	if(size>=sectorSize+sizeof(uint16_t)){
		// Background ID entry is present, load it
		self->sectorBackgroundID=bytesToUint16(&sectorData[sectorSize]);
		Editor_reloadBackground(self);
	}
	else if(self->background){
		SDL_DestroyTexture(self->background);
		self->background=0;
	}

	if(size>=sectorSize+2*sizeof(uint16_t))
		self->sectorUnknown1=bytesToUint16(&sectorData[sectorSize+sizeof(uint16_t)]);
	else
		self->sectorUnknown1=0;

	if(size>=sectorSize+3*sizeof(uint16_t)){
		// Music ID entry is present, load it
		self->sectorMusicID=bytesToUint16(&sectorData[sectorSize+2*sizeof(uint16_t)]);
	}
	else
		self->sectorMusicID=0;

	int metadataSize=(int)size-(sectorSize+3*sizeof(uint16_t));
	if(metadataSize>0){
		// Load rest of metadata so it won't get lost after saving
		if(self->sectorMetadata)
			free(self->sectorMetadata);
		self->sectorMetadataSize=metadataSize;
		self->sectorMetadata=malloc(self->sectorMetadataSize);
		memcpy(self->sectorMetadata,&sectorData[sectorSize+3*sizeof(uint16_t)],self->sectorMetadataSize);
	}
	else{
		if(self->sectorMetadata)
			free(self->sectorMetadata);
		self->sectorMetadata=0;
		self->sectorMetadataSize=0;
	}

	free(sectorData);
	printf("(Log) [Editor] Loaded sector %d,%d\n",x,y);
}
void Editor_save(Editor* self){
	char filename[512];
	snprintf(filename,512,"ASSETS/POZIOMY/%d,%d.poziom",self->sectorx,self->sectory);
	unsigned sectorSize=SECTOR_WIDTH*SECTOR_HEIGHT*sizeof(uint16_t);

	char sectorCopy[sectorSize+3*sizeof(uint16_t)+self->sectorMetadataSize];
	uint16ArrayToByteArray(self->sector,sectorCopy,sectorSize/sizeof(uint16_t));

	// Write metadata
	uint16ToBytes(self->sectorBackgroundID,&sectorCopy[sectorSize]); // Background ID
	uint16ToBytes(self->sectorUnknown1,&sectorCopy[sectorSize+sizeof(uint16_t)]);
	uint16ToBytes(self->sectorMusicID,&sectorCopy[sectorSize+2*sizeof(uint16_t)]); // Music ID
	if(self->sectorMetadataSize>0)
		memcpy(&sectorCopy[sectorSize+3*sizeof(uint16_t)],self->sectorMetadata,self->sectorMetadataSize);

	Data_updateFile(filename,sectorCopy,sectorSize+3*sizeof(uint16_t)+self->sectorMetadataSize);
	printf("(Log) [Editor] Saved file %s\n",filename);

	Overview_updateSectorPreview(&self->app->overview,self->sectorx,self->sectory,(char*)self->sector);
}
void Editor_reloadBackground(Editor *self){
	char filename[512];
	snprintf(filename,512,"ASSETS/GRAFIKA/TLA/%d.png",self->sectorBackgroundID);
	char* backgroundData;
	SDL_RWops* rw=Data_getFileRW(filename,&backgroundData);
	SDL_Surface* backgroundUnit=IMG_Load_RW(rw,0);
	SDL_FreeRW(rw);
	free(backgroundData);

	if(self->background)
		SDL_DestroyTexture(self->background);

	SDL_Surface* backgroundSurf=Editor_createBackground(self,backgroundUnit);
	self->background=SDL_CreateTextureFromSurface(self->renderer,backgroundSurf);
	SDL_FreeSurface(backgroundSurf);

	SDL_FreeSurface(backgroundUnit);
}
SDL_Surface* Editor_createBackground(Editor* self,SDL_Surface* backgroundUnit){
	SDL_Surface* background=SDL_CreateRGBSurface(0,SECTOR_WIDTH*TILE_SIZE,SECTOR_HEIGHT*TILE_SIZE,32,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	SDL_Rect rect;

	for(unsigned x=0; x<ceilf((SECTOR_WIDTH*TILE_SIZE)/(float)backgroundUnit->w); x++){
		for(unsigned y=0; y<ceilf((SECTOR_HEIGHT*TILE_SIZE)/(float)backgroundUnit->h); y++){
			rect.x=x*backgroundUnit->w;
			rect.y=y*backgroundUnit->h;
			SDL_BlitSurface(backgroundUnit,0,background,&rect);
		}
	}

	return background;
}
SDL_Surface* Editor_generateTilesetSurface(Editor* self){
	SDL_Surface* tilesetSurf=SDL_CreateRGBSurface(0,TILE_SIZE,TILE_COUNT*TILE_SIZE,32,0xFF000000,0x00FF0000,0x0000FF00,0x000000FF);
	if(!tilesetSurf){
		char errorMsg[1024];
		snprintf(errorMsg,1024,"Failed to create SDL_Surface: %s",SDL_GetError());
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,"PBEditor",errorMsg,0);
		exit(1);
	}
	SDL_Rect rect;
	rect.x=0;
	char filename[512];
	for(unsigned i=1; i<=TILE_COUNT; i++){
		snprintf(filename,512,"ASSETS/GRAFIKA/KLOCKI/%d.png",i);

		char* tileData;
		SDL_RWops* rw=Data_getFileRW(filename,&tileData);
		if(!tileData)
			continue;

		SDL_Surface* tile=IMG_Load_RW(rw,0);
		rect.y=(i-1)*TILE_SIZE;
		SDL_BlitSurface(tile,0,tilesetSurf,&rect);

		SDL_FreeRW(rw);
		free(tileData);
		SDL_FreeSurface(tile);
	}
	return tilesetSurf;
}