# PBEditor
## About
PBEditor is an unofficial level editor for [Perypetie Boba / Bob's Adventure](https://www.olekolek1000.com/pb) game.
## Project status
As for now PBEditor is able to:
- open, edit and save sector tile data
- delete, move and copy sectors
- create new sectors
- change sector background
- change sector music
- show map overview
- modify game to start in certain sector

Planned features for next version:
- support for latest Windows version of the game which has gamedata bundled with exe
- final touch ups

## Installing
Download binary package and follow instructions for your operating system, also before running make sure that gamedata file is next to PBEditor executable. It can be downloaded from linux64 distribution of the game.
### Linux distributions that use apt
```
$ sudo apt install libsdl2-2.0-0 libsdl2-image-2.0-0 libsdl2-ttf-2.0-0
$ tar xzfv <package>.tar.gz
$ cd <package>
$ ./PBEditor
```
### Fedora
```
$ sudo dnf install SDL2 SDL2_image SDL2_ttf
$ tar xzfv <package>.tar.gz
$ cd <package>
$ ./PBEditor
```
### OpenSUSE
```
$ sudo zypper install SDL2 SDL2_image SDL2_ttf
$ tar xzfv <package>.tar.gz
$ cd <package>
$ ./PBEditor
```
### Alpine Linux (binary package will be available in future)
```
$ sudo apk add sdl2 sdl2_image sdl2_ttf
$ tar xzfv <package>.tar.gz
$ cd <package>
$ ./PBEditor
```
### OpenBSD
```
$ sudo pkg_add SDL2 SDL2-image SDL2-ttf
$ tar xzfv <package>.tar.gz
$ cd <package>
$ ./PBEditor
```
### Windows
Extract package with your archiver of choice then enter extracted folder and run PBEditor.exe
## Contributing
Pull requests and issue submitting are welcome.
